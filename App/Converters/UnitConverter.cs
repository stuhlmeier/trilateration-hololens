﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace App.Converters
{
    public class UnitConverter : IValueConverter
    {
        public string Unit { get; set; }

        public UnitConverter(string unit) => Unit = unit;

        public UnitConverter() : this(null)
        {
        }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return Unit == null ? value.ToString() : $"{value} {Unit}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}